![Abu from Aladdin](http://img4.imagetitan.com/img4/small/13/13_abu.jpg "Abu from Aladdin")

# abu (Acquia Backup Utility)
  
## Requirements

* [HashBackup](http://www.hashbackup.com/) (build 1441 or later)
* [AWS IAM account with S3 write access](http://blogs.aws.amazon.com/security/post/Tx3VRSWZ6B3SHAV/Writing-IAM-Policies-How-to-grant-access-to-an-Amazon-S3-bucket)

## Installation

1. In your Acquia docroot git repository, create a folder called *backup* (same level as *docroot* and *hooks*)
2. In the *backup* folder add the contents of this repository folder (hb, abu.sh, dest.conf, etc.)
3. Make sure *hb* and *abu.sh* are set to executable (chmod +x)
4. Edit the *dest.conf* file and set your S3 access key, secret key, and bucket name
5. Add, commit, and push the new *backup* folder to Acquia Cloud
6. Use the Acquia Cloud Dashboard to create a daily cron job to run *backup* (see command below)
7. [optional] Login to the Acquia Cloud server via ssh and run other commands (see below)

## Commands

**backup [--verbose]**

* Basic Example: `/var/www/html/${AH_SITE_NAME}/backup/abu.sh backup &>> /var/log/sites/${AH_SITE_NAME}/logs/$(hostname -s)/hashbackup.log`
* Verbose Output Example: `/var/www/html/${AH_SITE_NAME}/backup/abu.sh backup --verbose`
 
**search *file-folder-name* **

* Basic Example: `/var/www/html/${AH_SITE_NAME}/backup/abu.sh search tulips.jpg`
* Regex Example: `/var/www/html/${AH_SITE_NAME}/backup/abu.sh search sites/default.*tulips_[0-9].*.jpg`
* Folder Example: `/var/www/html/${AH_SITE_NAME}/backup/abu.sh search thumbnail$`
* Database Example: `/var/www/html/${AH_SITE_NAME}/backup/abu.sh search ${AH_SITE_NAME}.*sql.gz$`
 
**restore */full/path/to/file-folder-name* [--rev #] [--origin]**

* Basic Example: `/var/www/html/${AH_SITE_NAME}/backup/abu.sh restore /mnt/gfs/d7a01dev/sites/default/files/tulips.jpg`
* Revision Example: `/var/www/html/${AH_SITE_NAME}/backup/abu.sh restore /mnt/gfs/d7a01dev/sites/default/files/readme.txt --rev 3`
* Overwrite Original Location Example: `/var/www/html/${AH_SITE_NAME}/backup/abu.sh restore /mnt/gfs/d7a01dev/sites/default/files/tulips.jpg --origin`

## Notes

* Default backup policy is 21 daily backups at a docroot level for both user files and databases.  You can [tweak the script](https://bitbucket.org/uitsweb/acquia-utils/src/master/backup/abu.sh?at=master&fileviewer=file-view-default#abu.sh-66) as needed with [a different policy](http://www.hashbackup.com/commands/retain).
* By default, restored files/folders are placed in */tmp/restored* unless *--origin* is used.
* The aim for the script was simplicity so advanced encryption key management and command passphrases were not used.  Feel free to review the [HashBackup documentation](http://www.hashbackup.com/commands) and implement them.
* By default, the S3 class is [Infrequent Access](https://aws.amazon.com/s3/storage-classes/) but you can [change it](https://gist.github.com/anonymous/4be2172f624d627ebbfb) if needed.
