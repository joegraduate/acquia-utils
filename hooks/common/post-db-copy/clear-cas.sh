#!/bin/sh
#
# Cloud Hook: clear-cas
#
# Clear cas token data (if exists).

site="$1"
target_env="$2"
db_name="$3"
source_env="$4"

if [ "$AH_SITE_ENVIRONMENT" != "ra" ]; then
  cas=`drush @$site.$target_env sql-query "SHOW TABLES LIKE '%cas_login_data'"  --database=$db_name`
  if [ ! -z "$cas" ]; then
    drush @$site.$target_env sql-query "TRUNCATE {cas_login_data}" --db-prefix --database=$db_name
    drush @$site.$target_env cache-clear all --yes
  fi
fi
